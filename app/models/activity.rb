class Activity < ActiveRecord::Base
	belongs_to :user
	has_many :subscriptions
	has_many :followers, :through => :subscriptions , :source => :user
end

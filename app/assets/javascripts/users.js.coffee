# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready( ->
	$("#my_activities").hide();
	$("#my_todos").show();
	
	$("#todos_link").click( ->
		$("#my_activities").hide();
		$("#my_todos").show();
	) 
	
	$("#activities_link").click( ->
		$("#my_todos").hide();
		$("#my_activities").show();
	)


)
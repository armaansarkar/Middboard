class UsersController < ApplicationController
	def show
		@user = User.find(params[:id])
		@todos = @user.todos
	end

	def my_activities
		@user = User.find(params[:id])
		@activities = @user.activities
	end

	def my_todos
		@user = User.find(params[:id])
		@todos = @user.todos
	end
end

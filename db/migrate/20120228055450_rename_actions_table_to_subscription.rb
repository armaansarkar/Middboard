class RenameActionsTableToSubscription < ActiveRecord::Migration
  def change
  	rename_table :actions, :subscription
  end
end

class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.integer :user_id
      t.integer :action_id
      t.datetime :created_at

      t.timestamps
    end
  end
end

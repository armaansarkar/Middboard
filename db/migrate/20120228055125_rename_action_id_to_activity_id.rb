class RenameActionIdToActivityId < ActiveRecord::Migration
	def change
		rename_column :actions, :action_id, :activity_id
	end
end
